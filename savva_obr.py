import os
import xlrd
import dbf
import calendar
from datetime import datetime
from shutil import copyfile

directory = "./xls/"
files = os.listdir(directory)

date = datetime.now()
last_day = date.replace(day=calendar.monthrange(date.year, date.month)[1])
ex_file_name = 'EDSB_' + last_day.strftime('%d%m%y') + '.dbf'

prefix = 'EDSB001'


def calc_massive():
    ins_rowcount = 0
    total_sum = 0
    ent_dict = []
    for f in files:
        if f[-4:].upper() == '.DBF':
            dbf_file = dbf.Table(directory + f)
            dbf_file.open()
            for record in dbf_file:
                ent_dict.append([
                    datetime.strftime(record[0], '%d.%m.%Y'), record[1],
                    record[2]
                ])
                total_sum += record[2]
        else:
            rd = xlrd.open_workbook(directory + f, formatting_info=True)
            sheet = rd.sheet_by_index(0)
            for rowNum in range(sheet.nrows):
                row = sheet.row_values(rowNum)
                if row[0][:3] == "100":
                    p_acc = row[0]
                    p_ent_date = sheet.row_values(rowNum + 1)[5][:10]
                    p_value = str(sheet.row_values(rowNum + 1)[11])
                    ent_dict.append([p_ent_date, p_acc, p_value])
                    ins_rowcount += 1
                    total_sum += sheet.row_values(rowNum + 1)[11]
    return (ent_dict, total_sum)


def make_dbf(rows_massive):
    copyfile('EDSB_DDMMYY.dbf', ex_file_name)
    table = dbf.Table(ex_file_name)
    table.open(mode=dbf.READ_WRITE)
    for r in rows_massive:
        table.append((datetime.strptime(r[0], '%d.%m.%Y'), r[1], r[2], None,
                      None, prefix))


def main():
    rows, total = calc_massive()
    make_dbf(rows)
    print('Done, imported rows = {}, total sum = {}'.format(len(rows), total))


if __name__ == '__main__':
    main()
