import os
import xlrd
import sqlite3
from datetime import datetime
import dbf

directory = "./xls/"
files = os.listdir(directory)
conn = sqlite3.connect("main.db")
c = conn.cursor()
p_house = "savva"
ent_dict = []


def clear():
    c.execute("DELETE FROM tEntry")
    conn.commit()


def calc():
    ins_rowcount = 0
    for f in files:
        rd = xlrd.open_workbook(directory + f, formatting_info=True)
        sheet = rd.sheet_by_index(0)
        for rowNum in range(sheet.nrows):
            row = sheet.row_values(rowNum)
            if row[0][:3] == "100":
                p_acc = row[0]
                p_imp_date = datetime.now().strftime("%d.%m.%y")
                p_ent_date = sheet.row_values(rowNum + 1)[5][:10]
                p_value = str(sheet.row_values(rowNum + 1)[11])

                c.execute(
                    "INSERT INTO tEntry(house, acc, impdate, entDate, value, fName) values('{}',{},'{}','{}',{},'{}')".
                    format(p_house, p_acc, p_imp_date, p_ent_date, p_value, f))
                ins_rowcount += c.rowcount
    conn.commit()
    return (str(ins_rowcount))


def make_dbf():
    table = dbf.Table("new", "DATE C(100); ACC N(10,0); VALUE N(10,0)")
    table.open(mode=dbf.READ_WRITE)
    for r in c.execute('select entDate, acc, value from tEntry'):
        table.append(r)


def main():
    clear()
    rows = calc()
    print("Done, inserted row = {}".format(rows))
    make_dbf()
    print("Done, dbf created")
    conn.close()


if __name__ == '__main__':
    main()
